<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-api-geo-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvApiGeo;

/**
 * ApiFrGouvApiGeoDepartement class file.
 * 
 * This is a simple implementation of the ApiFrGouvApiGeoDepartementInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvApiGeoDepartement implements ApiFrGouvApiGeoDepartementInterface
{
	
	/**
	 * The code of the departement.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The code of the parent region.
	 * 
	 * @var string
	 */
	protected string $_codeRegion;
	
	/**
	 * The nom of the departement.
	 * 
	 * @var string
	 */
	protected string $_nom;
	
	/**
	 * Constructor for ApiFrGouvApiGeoDepartement with private members.
	 * 
	 * @param string $code
	 * @param string $codeRegion
	 * @param string $nom
	 */
	public function __construct(string $code, string $codeRegion, string $nom)
	{
		$this->setCode($code);
		$this->setCodeRegion($codeRegion);
		$this->setNom($nom);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the code of the departement.
	 * 
	 * @param string $code
	 * @return ApiFrGouvApiGeoDepartementInterface
	 */
	public function setCode(string $code) : ApiFrGouvApiGeoDepartementInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the departement.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the code of the parent region.
	 * 
	 * @param string $codeRegion
	 * @return ApiFrGouvApiGeoDepartementInterface
	 */
	public function setCodeRegion(string $codeRegion) : ApiFrGouvApiGeoDepartementInterface
	{
		$this->_codeRegion = $codeRegion;
		
		return $this;
	}
	
	/**
	 * Gets the code of the parent region.
	 * 
	 * @return string
	 */
	public function getCodeRegion() : string
	{
		return $this->_codeRegion;
	}
	
	/**
	 * Sets the nom of the departement.
	 * 
	 * @param string $nom
	 * @return ApiFrGouvApiGeoDepartementInterface
	 */
	public function setNom(string $nom) : ApiFrGouvApiGeoDepartementInterface
	{
		$this->_nom = $nom;
		
		return $this;
	}
	
	/**
	 * Gets the nom of the departement.
	 * 
	 * @return string
	 */
	public function getNom() : string
	{
		return $this->_nom;
	}
	
}
