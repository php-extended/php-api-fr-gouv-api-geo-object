<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-api-geo-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvApiGeo;

use InvalidArgumentException;
use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\GeoJson\GeoJsonFeature;
use PhpExtended\GeoJson\GeoJsonFeatureCollection;
use PhpExtended\GeoJson\GeoJsonFeatureInterface;
use PhpExtended\GeoJson\GeoJsonReifierConfiguration;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;

/**
 * ApiFrGouvApiGeoApiEndpoint class file.
 * 
 * This class represents the geo.api.gouv.fr endpoint.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiFrGouvApiGeoEndpoint implements ApiFrGouvApiGeoEndpointInterface
{
	
	public const HOST = 'https://geo.api.gouv.fr/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * Builds a new Endpoint with its dependancies.
	 *
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 * @throws InvalidArgumentException
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		
		$configuration = new GeoJsonReifierConfiguration();
		// $configuration->setImplementation(GeoJsonGeometryInterface::class, GeoJsonPolygon::class);
		$configuration->setIterableInnerTypes(ApiFrGouvApiGeoCommune::class, ['codesPostaux'], 'string');
		$this->_reifier->setConfiguration($configuration);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpointInterface::getRegions()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRegions() : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'regions');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiFrGouvApiGeoRegion::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpointInterface::getRegion()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRegion(string $codeRegion) : ApiFrGouvApiGeoRegionInterface
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'regions/'.\urlencode($codeRegion));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiFrGouvApiGeoRegion::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpointInterface::getDepartements()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDepartements() : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'departements');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiFrGouvApiGeoDepartement::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpointInterface::getDepartement()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDepartement(string $codeDepartement) : ApiFrGouvApiGeoDepartementInterface
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'departements/'.\urlencode($codeDepartement));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiFrGouvApiGeoDepartement::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpointInterface::getDepartementsFromRegion()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDepartementsFromRegion(string $codeRegion) : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'regions/'.\urlencode($codeRegion).'/departements');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiFrGouvApiGeoDepartement::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpointInterface::getCommunes()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCommunes() : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'communes');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiFrGouvApiGeoCommune::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpointInterface::getCommunesFromDepartement()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCommunesFromDepartement(string $codeDepartement) : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'departements/'.\urlencode($codeDepartement).'/communes?format=geojson&geometry=contour');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		/** @var GeoJsonFeatureCollection $featureCollection */
		$featureCollection = $this->_reifier->reify(GeoJsonFeatureCollection::class, $json->provideOne());
		
		$communes = [];
		
		/** @var GeoJsonFeature $feature */
		foreach($featureCollection->getFeatures() as $feature)
		{
			$communes[] = $this->processFeatureIntoCommune($feature);
		}
		
		return $communes;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpointInterface::getCommune()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCommune(string $codeCommune) : ApiFrGouvApiGeoCommuneInterface
	{
		
		$uri = $this->_uriFactory->createUri(self::HOST.'communes/'.\urlencode($codeCommune).'?format=geojson&geometry=contour');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		/** @var GeoJsonFeature $feature */
		$feature = $this->_reifier->reify(GeoJsonFeature::class, $json->provideOne());
		
		return $this->processFeatureIntoCommune($feature);
	}
	
	/**
	 * Transforms a feature into a commune.
	 * 
	 * @param GeoJsonFeatureInterface $feature
	 * @return ApiFrGouvApiGeoCommune
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	protected function processFeatureIntoCommune(GeoJsonFeatureInterface $feature) : ApiFrGouvApiGeoCommuneInterface
	{
		$properties = [
			'geometry' => $feature->getGeometry(),
		];
		
		foreach($feature->getProperties() as $name => $property)
		{
			if(\is_array($property) && isset($property[0]))
			{
				$properties[$name] = $property[0];
			}
		}
		
		return $this->_reifier->reify(ApiFrGouvApiGeoCommune::class, $properties);
	}
	
}
