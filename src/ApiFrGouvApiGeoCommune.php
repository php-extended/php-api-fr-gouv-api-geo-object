<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-api-geo-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvApiGeo;

use PhpExtended\GeoJson\GeoJsonGeometryInterface;

/**
 * ApiFrGouvApiGeoCommune class file.
 * 
 * This is a simple implementation of the ApiFrGouvApiGeoCommuneInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvApiGeoCommune implements ApiFrGouvApiGeoCommuneInterface
{
	
	/**
	 * The code of the commune.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The code departement of the commune.
	 * 
	 * @var string
	 */
	protected string $_codeDepartement;
	
	/**
	 * The code region of the commune.
	 * 
	 * @var string
	 */
	protected string $_codeRegion;
	
	/**
	 * The nom of the commune.
	 * 
	 * @var string
	 */
	protected string $_nom;
	
	/**
	 * The siren of the commune, if any.
	 * 
	 * @var ?string
	 */
	protected ?string $_siren = null;
	
	/**
	 * The code epci of the commune.
	 * 
	 * @var ?string
	 */
	protected ?string $_codeEpci = null;
	
	/**
	 * The codes postaux for this commune.
	 * 
	 * @var array<int, string>
	 */
	protected array $_codesPostaux = [];
	
	/**
	 * The population of this commune.
	 * 
	 * @var ?int
	 */
	protected ?int $_population = null;
	
	/**
	 * This interface specifies a geometry GeoJSON object.
	 * 
	 * @var ?GeoJsonGeometryInterface
	 */
	protected ?GeoJsonGeometryInterface $_geometry = null;
	
	/**
	 * Constructor for ApiFrGouvApiGeoCommune with private members.
	 * 
	 * @param string $code
	 * @param string $codeDepartement
	 * @param string $codeRegion
	 * @param string $nom
	 * @param array<int, string> $codesPostaux
	 */
	public function __construct(string $code, string $codeDepartement, string $codeRegion, string $nom, array $codesPostaux)
	{
		$this->setCode($code);
		$this->setCodeDepartement($codeDepartement);
		$this->setCodeRegion($codeRegion);
		$this->setNom($nom);
		$this->setCodesPostaux($codesPostaux);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the code of the commune.
	 * 
	 * @param string $code
	 * @return ApiFrGouvApiGeoCommuneInterface
	 */
	public function setCode(string $code) : ApiFrGouvApiGeoCommuneInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the commune.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the code departement of the commune.
	 * 
	 * @param string $codeDepartement
	 * @return ApiFrGouvApiGeoCommuneInterface
	 */
	public function setCodeDepartement(string $codeDepartement) : ApiFrGouvApiGeoCommuneInterface
	{
		$this->_codeDepartement = $codeDepartement;
		
		return $this;
	}
	
	/**
	 * Gets the code departement of the commune.
	 * 
	 * @return string
	 */
	public function getCodeDepartement() : string
	{
		return $this->_codeDepartement;
	}
	
	/**
	 * Sets the code region of the commune.
	 * 
	 * @param string $codeRegion
	 * @return ApiFrGouvApiGeoCommuneInterface
	 */
	public function setCodeRegion(string $codeRegion) : ApiFrGouvApiGeoCommuneInterface
	{
		$this->_codeRegion = $codeRegion;
		
		return $this;
	}
	
	/**
	 * Gets the code region of the commune.
	 * 
	 * @return string
	 */
	public function getCodeRegion() : string
	{
		return $this->_codeRegion;
	}
	
	/**
	 * Sets the nom of the commune.
	 * 
	 * @param string $nom
	 * @return ApiFrGouvApiGeoCommuneInterface
	 */
	public function setNom(string $nom) : ApiFrGouvApiGeoCommuneInterface
	{
		$this->_nom = $nom;
		
		return $this;
	}
	
	/**
	 * Gets the nom of the commune.
	 * 
	 * @return string
	 */
	public function getNom() : string
	{
		return $this->_nom;
	}
	
	/**
	 * Sets the siren of the commune, if any.
	 * 
	 * @param ?string $siren
	 * @return ApiFrGouvApiGeoCommuneInterface
	 */
	public function setSiren(?string $siren) : ApiFrGouvApiGeoCommuneInterface
	{
		$this->_siren = $siren;
		
		return $this;
	}
	
	/**
	 * Gets the siren of the commune, if any.
	 * 
	 * @return ?string
	 */
	public function getSiren() : ?string
	{
		return $this->_siren;
	}
	
	/**
	 * Sets the code epci of the commune.
	 * 
	 * @param ?string $codeEpci
	 * @return ApiFrGouvApiGeoCommuneInterface
	 */
	public function setCodeEpci(?string $codeEpci) : ApiFrGouvApiGeoCommuneInterface
	{
		$this->_codeEpci = $codeEpci;
		
		return $this;
	}
	
	/**
	 * Gets the code epci of the commune.
	 * 
	 * @return ?string
	 */
	public function getCodeEpci() : ?string
	{
		return $this->_codeEpci;
	}
	
	/**
	 * Sets the codes postaux for this commune.
	 * 
	 * @param array<int, string> $codesPostaux
	 * @return ApiFrGouvApiGeoCommuneInterface
	 */
	public function setCodesPostaux(array $codesPostaux) : ApiFrGouvApiGeoCommuneInterface
	{
		$this->_codesPostaux = $codesPostaux;
		
		return $this;
	}
	
	/**
	 * Gets the codes postaux for this commune.
	 * 
	 * @return array<int, string>
	 */
	public function getCodesPostaux() : array
	{
		return $this->_codesPostaux;
	}
	
	/**
	 * Sets the population of this commune.
	 * 
	 * @param ?int $population
	 * @return ApiFrGouvApiGeoCommuneInterface
	 */
	public function setPopulation(?int $population) : ApiFrGouvApiGeoCommuneInterface
	{
		$this->_population = $population;
		
		return $this;
	}
	
	/**
	 * Gets the population of this commune.
	 * 
	 * @return ?int
	 */
	public function getPopulation() : ?int
	{
		return $this->_population;
	}
	
	/**
	 * Sets this interface specifies a geometry GeoJSON object.
	 * 
	 * @param ?GeoJsonGeometryInterface $geometry
	 * @return ApiFrGouvApiGeoCommuneInterface
	 */
	public function setGeometry(?GeoJsonGeometryInterface $geometry) : ApiFrGouvApiGeoCommuneInterface
	{
		$this->_geometry = $geometry;
		
		return $this;
	}
	
	/**
	 * Gets this interface specifies a geometry GeoJSON object.
	 * 
	 * @return ?GeoJsonGeometryInterface
	 */
	public function getGeometry() : ?GeoJsonGeometryInterface
	{
		return $this->_geometry;
	}
	
}
