<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-api-geo-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvApiGeo;

/**
 * ApiFrGouvApiGeoRegion class file.
 * 
 * This is a simple implementation of the ApiFrGouvApiGeoRegionInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvApiGeoRegion implements ApiFrGouvApiGeoRegionInterface
{
	
	/**
	 * The code of the region.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The nom of the region.
	 * 
	 * @var string
	 */
	protected string $_nom;
	
	/**
	 * Constructor for ApiFrGouvApiGeoRegion with private members.
	 * 
	 * @param string $code
	 * @param string $nom
	 */
	public function __construct(string $code, string $nom)
	{
		$this->setCode($code);
		$this->setNom($nom);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the code of the region.
	 * 
	 * @param string $code
	 * @return ApiFrGouvApiGeoRegionInterface
	 */
	public function setCode(string $code) : ApiFrGouvApiGeoRegionInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the region.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the nom of the region.
	 * 
	 * @param string $nom
	 * @return ApiFrGouvApiGeoRegionInterface
	 */
	public function setNom(string $nom) : ApiFrGouvApiGeoRegionInterface
	{
		$this->_nom = $nom;
		
		return $this;
	}
	
	/**
	 * Gets the nom of the region.
	 * 
	 * @return string
	 */
	public function getNom() : string
	{
		return $this->_nom;
	}
	
}
