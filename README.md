# php-extended/php-api-fr-gouv-api-geo-object

A library that implements the php-extended/php-api-fr-gouv-api-geo-interface library.

![coverage](https://gitlab.com/php-extended/php-api-fr-gouv-api-geo-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-gouv-api-geo-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-gouv-api-geo-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\GeoApiGouvFr\GeoApiGouvFrEndpoint;

/** @var $client \Psr\Http\Client\ClientInterface */

$endpoint = new GeoApiGouvFrEndpoint($client);

foreach($endpoint->getRegions() as $region)
{
	// do something with regions
}

foreach($endpoint->getDepartements() as $departement)
{
	// do something with departements
}

foreach($endpoint->getCommunes() as $commune)
{
	// do something with communes
}

```

Note that the `$endpoint->getCommunes()` method does not recover
the polygons about the communes. To have them, you have to get communes
from each departement like so :

```php

foreach($endpoint->getDepartements() as $departement)
{
	foreach($endpoint->getCommunesFromDepartement($departement->getCode()) as $commune)
	{
		$commune->getGeometry(); // polygon or multi polygon
	}
}

```


## License

MIT (See [license file](LICENSE)).
