<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-api-geo-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvApiGeo\Test;

use PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoCommune;
use PhpExtended\GeoJson\GeoJsonGeometryInterface;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvApiGeoCommuneTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoCommune
 * @internal
 * @small
 */
class ApiFrGouvApiGeoCommuneTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvApiGeoCommune
	 */
	protected ApiFrGouvApiGeoCommune $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetCode() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCode());
		$expected = 'qsdfghjklm';
		$this->_object->setCode($expected);
		$this->assertEquals($expected, $this->_object->getCode());
	}
	
	public function testGetCodeDepartement() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCodeDepartement());
		$expected = 'qsdfghjklm';
		$this->_object->setCodeDepartement($expected);
		$this->assertEquals($expected, $this->_object->getCodeDepartement());
	}
	
	public function testGetCodeRegion() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCodeRegion());
		$expected = 'qsdfghjklm';
		$this->_object->setCodeRegion($expected);
		$this->assertEquals($expected, $this->_object->getCodeRegion());
	}
	
	public function testGetNom() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNom());
		$expected = 'qsdfghjklm';
		$this->_object->setNom($expected);
		$this->assertEquals($expected, $this->_object->getNom());
	}
	
	public function testGetSiren() : void
	{
		$this->assertNull($this->_object->getSiren());
		$expected = 'qsdfghjklm';
		$this->_object->setSiren($expected);
		$this->assertEquals($expected, $this->_object->getSiren());
	}
	
	public function testGetCodeEpci() : void
	{
		$this->assertNull($this->_object->getCodeEpci());
		$expected = 'qsdfghjklm';
		$this->_object->setCodeEpci($expected);
		$this->assertEquals($expected, $this->_object->getCodeEpci());
	}
	
	public function testGetCodesPostaux() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getCodesPostaux());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setCodesPostaux($expected);
		$this->assertEquals($expected, $this->_object->getCodesPostaux());
	}
	
	public function testGetPopulation() : void
	{
		$this->assertNull($this->_object->getPopulation());
		$expected = 25;
		$this->_object->setPopulation($expected);
		$this->assertEquals($expected, $this->_object->getPopulation());
	}
	
	public function testGetGeometry() : void
	{
		$this->assertNull($this->_object->getGeometry());
		$expected = $this->getMockBuilder(GeoJsonGeometryInterface::class)->disableOriginalConstructor()->getMock();
		$this->_object->setGeometry($expected);
		$this->assertEquals($expected, $this->_object->getGeometry());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvApiGeoCommune('azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', ['azertyuiop']);
	}
	
}
