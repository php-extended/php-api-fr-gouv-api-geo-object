<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-api-geo-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoCommune;
use PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoCommuneInterface;
use PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpoint;
use PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpointInterface;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiFrGouvApiGeoEndpointTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiFrGouvApiGeoEndpointTest extends TestCase
{
	
	/**
	 * The endpoint to test.
	 * 
	 * @var ApiFrGouvApiGeoEndpointInterface
	 */
	protected ApiFrGouvApiGeoEndpointInterface $_endpoint;
	
	public function testGetRegions() : void
	{
		foreach($this->_endpoint->getRegions() as $region)
		{
			/** @var PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoRegionInterface $region */
			$this->assertNotEmpty($region->getCode());
			$this->assertNotEmpty($region->getNom());
		}
	}
	
	public function testGetRegion() : void
	{
		$region = $this->_endpoint->getRegion('84');
		$this->assertNotEmpty($region->getCode());
		$this->assertNotEmpty($region->getNom());
	}
	
	public function testGetDepartements() : void
	{
		foreach($this->_endpoint->getDepartements() as $departement)
		{
			/** @var PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoDepartementInterface $departement */
			$this->assertNotEmpty($departement->getCode());
			$this->assertNotEmpty($departement->getCodeRegion());
			$this->assertNotEmpty($departement->getNom());
		}
	}
	
	public function testGetDepartementsFromRegion() : void
	{
		foreach($this->_endpoint->getDepartementsFromRegion('75') as $departement)
		{
			/** @var PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoDepartementInterface $departement */
			$this->assertNotEmpty($departement->getCode());
			$this->assertNotEmpty($departement->getCodeRegion());
			$this->assertNotEmpty($departement->getNom());
		}
	}
	
	public function testGetDepartement() : void
	{
		$departement = $this->_endpoint->getDepartement('2A');
		$this->assertNotEmpty($departement->getCode());
		$this->assertNotEmpty($departement->getCodeRegion());
		$this->assertNotEmpty($departement->getNom());
	}
	
	public function testGetCommune() : void
	{
		$commune = $this->_endpoint->getCommune('03001');
		$this->assertNotEmpty($commune->getCode());
		$this->assertNotEmpty($commune->getCodeDepartement());
		$this->assertNotEmpty($commune->getCodeRegion());
		$this->assertNotEmpty($commune->getCodesPostaux());
		$this->assertNotEmpty($commune->getNom());
		$this->assertNotNull($commune->getPopulation());
		$this->assertNotEmpty($commune->getGeometry());
	}
	
	public function testGetCommuneFromDepartement() : void
	{
		/** @var ApiFrGouvApiGeoCommune $commune */
		foreach($this->_endpoint->getCommunesFromDepartement('2A') as $commune)
		{
			$this->assertNotEmpty($commune->getCode());
			$this->assertNotEmpty($commune->getCodeDepartement());
			$this->assertNotEmpty($commune->getCodeRegion());
			$this->assertNotEmpty($commune->getCodesPostaux());
			$this->assertNotEmpty($commune->getNom());
			// population may be missing
			$this->assertNotEmpty($commune->getGeometry());
		}
	}
	
	public function testGetCommunes() : void
	{
		/** @var ApiFrGouvApiGeoCommuneInterface $commune */
		foreach($this->_endpoint->getCommunes() as $commune)
		{
			$this->assertNotEmpty($commune->getCode());
			// code departement may be missing
			// code region may be missing
			// codes postaux may be empty
			$this->assertNotEmpty($commune->getNom());
			// population may be missing
			// geometry is missing
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$options = ['http' => ['user_agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0']];
				$data = \file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
				$body = new StringStream($data);
				
				return (new Response())->withBody($body);
			}
		};
		
		$this->_endpoint = new ApiFrGouvApiGeoEndpoint($client);
	}
	
}
