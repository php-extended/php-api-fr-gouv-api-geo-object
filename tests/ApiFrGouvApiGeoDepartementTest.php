<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-api-geo-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvApiGeo\Test;

use PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoDepartement;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvApiGeoDepartementTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoDepartement
 * @internal
 * @small
 */
class ApiFrGouvApiGeoDepartementTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvApiGeoDepartement
	 */
	protected ApiFrGouvApiGeoDepartement $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetCode() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCode());
		$expected = 'qsdfghjklm';
		$this->_object->setCode($expected);
		$this->assertEquals($expected, $this->_object->getCode());
	}
	
	public function testGetCodeRegion() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCodeRegion());
		$expected = 'qsdfghjklm';
		$this->_object->setCodeRegion($expected);
		$this->assertEquals($expected, $this->_object->getCodeRegion());
	}
	
	public function testGetNom() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNom());
		$expected = 'qsdfghjklm';
		$this->_object->setNom($expected);
		$this->assertEquals($expected, $this->_object->getNom());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvApiGeoDepartement('azertyuiop', 'azertyuiop', 'azertyuiop');
	}
	
}
